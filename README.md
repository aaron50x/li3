Lithium Li3 Docker Image 
========================

This docker image is packed with php 5.6 plus required packages (including mongodb driver) to start working on [li3 RAD framework](http://li3.me)

## User

As some restriction from Composer a user called li3 was created and its home dir is `/home/li3` it's recommended to use this user

```bash
docker run -u li3 unionofrad/li3
```

## Volume

A location inside image is a volume set in Dockerfile, this is `/home/li3/app`, so it's recommended to use when working with external sources

## Usage

### Simple usage

In this simple example, port 8080 is exposed, but you can expose any port you use in your development:

```bash
user@host:~$ docker run -it -u li3 -p 8080:8080 unionofrad/li3 bash
```

once running and in interactive mode, your workdir is /home/li3/app

```bash
li3@cd7898df832e:~/app$
``` 

now you can start creating your sample app, by executing composer command
more info http://li3.me/docs/book/manual/1.x/installation/

```bash
li3@cd7898df832e:~/app$ composer create-project --prefer-dist unionofrad/framework .
```

you can now expose the php built-in development server

```bash
li3@cd7898df832e:~/app$ php -S 0.0.0.0:8080 -t app/webroot index.php
```

In your browser you can go directly to http://localhost:8080 and see your li3 app, up and running :)

### External Source

If you need to link the source of your li3 app to a dir in your host, you can base your container creation installation in following script:

Here is assumed that your sources exist in `` /home/user/li3/project ``

```bash
docker run -it -u li3 -p 8080:8080 -v /path/to/project/root:/home/li3/app unionofrad/li3 php -S 0.0.0.0:8080 -t app/webroot index.php
```

### Alpine Tag

You can use the alpine lightweight version by using the alpine tag, example:

```bash
docker run -it -u li3 -p 8080:8080 -v /home/user/li3/project:/home/li3/app unionofrad/li3:alpine php -S 0.0.0.0:8080 -t app/webroot index.php
```

If you want to enter in interactive mode with this tag you must use ``sh`` instead:

```bash
docker run -it -u li3 -p 8080:8080 unionofrad/li3 sh
```

## Feedback

Any feedback is welcome to ![email](email.gif)